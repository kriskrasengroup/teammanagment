# TeamManagment

Project Description

Design and implement a Tasks Management console application.

The application will be used by a small team of developers, who need to keep track of all the tasks, surrounding a software product they are building.

Functional Requirements

The application must support multiple teams.

Each team must have a name, members, and boards.  The name must be unique in the application.

 The name is a string between 5 and 15 symbols.

Each member must have a name, list of tasks and activity history.  The name must be unique in the application.

 The name is a string between 5 and 15 symbols.

Each board must have a name, list of tasks and activity history.  Name must be unique in the team.

 Name is a string between 5 and 10 symbols.

There are 3 types of tasks: bug, story, and feedback.

Bug

Bugs must have an ID, a title, a description, a list of steps to reproduce it, a priority, a severity, a status, an assignee, a list of comments and a list of changes history.

 Title is a string between 10 and 50 symbols.

 Description is a string between 10 and 500 symbols.

 Steps to reproduce is a list of strings.

 Priority is one of the following: High, Medium, or Low.

 Severity is one of the following: Critical, Major, or Minor.

 Status is one of the following: Active or Fixed.

 Assignee is a member from the team.

 Comments is a list of comments (string messages with an author).

 History is a list of all changes (string messages) that were done to the bug.

Story

Stories must have an ID, a title, a description, a priority, a size, a status, an assignee, a list of comments and a list of changes history.

 Title is a string between 10 and 50 symbols.

 Description is a string between 10 and 500 symbols.

 Priority is one of the following: High, Medium, or Low.

 Size is one of the following: Large, Medium, or Small.

 Status is one of the following: Not Done, InProgress, or Done.

 Assignee is a member from the team.

 Comments is a list of comments (string messages with author).

 History is a list of all changes (string messages) that were done to the story.

Feedback

Feedbacks must have an ID, a title, a description, a rating, a status, a list of comments and a list of changes history.

 Title is a string between 10 and 50 symbols.

 Description is a string between 10 and 500 symbols.

 Rating is an integer.

 Status is one of the following: New, Unscheduled, Scheduled, or Done.

 Comments is a list of comments (string messages with author).

 History is a list of all changes (string messages) that were done to the feedback.

Note: IDs of tasks must be unique in the application i.e., if we have a bug with ID 42 then we cannot have a story or a feedback with ID 42.

Operations

The application must support the following operations:

 Create a new person.

 Show all people.

 Show person's activity.

 Create a new team.

 Show all teams.

 Show team's activity.

 Add person to team.

 Show all team members.

 Create a new board in a team.

 Show all team boards.

 Show board's activity.

 Create a new Bug/Story/Feedback in a board.

 Change the Priority/Severity/Status of a bug.

 Change the Priority/Size/Status of a story.

 Change the Rating/Status of a feedback.

 Assign/Unassign a task to a person.  Add comment to a task.

 Listing:

o List all tasks (display the most important info).

 Filter by title

 Sort by title

o List bugs/stories/feedback only.

 Filter by status and/or assignee

 Sort by title/priority/severity/size/rating (depending on the task type) o List tasks with assignee.

 Filter by status and/or assignee

 Sort by title