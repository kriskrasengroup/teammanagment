﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TaskManagement.Commands;
using TaskManagement.Contracts;
using TaskManagement.Core.Contracts;

namespace TeamManagment.UnitTests
{
    [TestClass]
    public class ShowUserActivityCommandTests
    {
        [TestMethod]
        public void Should_Throw_WhenParametersLessThanExpected()
        {
            var repository = new Mock<IRepository>();
            var commandParameters = new List<string>();
            var test = new ShowUserActivityCommand(commandParameters, repository.Object);
            Assert.ThrowsException<ArgumentException>(() => test.Execute());
        }
        [TestMethod]
        public void Should_Throw_WhenTeamDoesNotExists()
        {
            var teamName = "TestTeam";
            var boardName = "TestBoard";
            var commandParameters = new List<string> { teamName, boardName };
            var team = new Mock<ITeam>();
            team.SetupGet(x => x.Name).Returns(teamName);
            var repository = new Mock<IRepository>();
            repository.Setup(x => x.UserExists(teamName)).Returns(false);
            var test = new ShowUserActivityCommand(commandParameters, repository.Object);
            Assert.ThrowsException<ArgumentException>(() => test.Execute());
        }

        [TestMethod]
        public void Should_Print()
        {
            var teamName = "TestTeam";
            var boardName = "TestBoard";
            var bugName = "TestBug";
            var commandParameters = new List<string> { teamName, boardName };

            var user = new Mock<IUser>();
            user.SetupGet(x => x.Name).Returns(teamName);

            var bug = new Mock<IBug>();
            bug.SetupGet(x => x.Title).Returns(bugName);
            bug.SetupGet(x => x.Id).Returns(1);

            var board = new Mock<IBoard>();
            board.SetupGet(x => x.Name).Returns(boardName);
            board.SetupGet(x => x.WorkItems).Returns(new List<IWorkItem> { bug.Object });

            var team = new Mock<ITeam>();
            team.SetupGet(x => x.Name).Returns(teamName);
            team.SetupGet(x => x.Boards).Returns(new List<IBoard> { board.Object });

            var activity = new Mock<IActivityHistory>();
            activity.SetupGet(x => x.Team).Returns(team.Object);
            activity.SetupGet(x => x.Board).Returns(board.Object);
            activity.SetupGet(x => x.WorkItem).Returns(bug.Object);
            activity.SetupGet(x => x.User).Returns(user.Object);

            var repository = new Mock<IRepository>();
            repository.Setup(x => x.UserExists(teamName)).Returns(true);
            repository.SetupGet(x => x.Teams).Returns(new List<ITeam> { team.Object });
            repository.SetupGet(x => x.ActivityHistories).Returns(new List<IActivityHistory> { activity.Object });           
            var activityToPrint = repository.Object.ActivityHistories.Where(x => x.User.Name == teamName).ToList();
            var test = new ShowUserActivityCommand(commandParameters, repository.Object);
            var actual = test.Execute();
            var sb = new StringBuilder();
            sb.AppendLine($"User: {teamName}");
            foreach (var item in activityToPrint)
            {
                sb.AppendLine($"Has WorkItem: {item.WorkItem.Title} with ID: {item.WorkItem.Id}");
                sb.AppendLine($"Description: {item.WorkItem.Description}");
                sb.AppendLine("  ----------  ");
            }
            var expected = sb.ToString().TrimEnd();
            Assert.AreEqual(actual, expected);
        }
    }
}
