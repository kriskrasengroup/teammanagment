﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using TaskManagement.Contracts;
using TaskManagement.Core;
using TaskManagement.Core.Contracts;

namespace TeamManagment.UnitTests
{
    [TestClass]
    public class RepositoryTests
    {
        [TestMethod]
        public void Should_CreateAllLists()
        {
            var userName = "TestUser";
            var teamName = "TestTeam";
            var user = new Mock<IUser>();
            user.SetupGet(x => x.Name).Returns(userName);
            var team = new Mock<ITeam>();
            team.SetupGet(x => x.Name).Returns(teamName);
            var board = new Mock<IBoard>();
            var workItem = new Mock<IWorkItem>();
           
            var repository = new Repository();
            repository.AddWorkItem(workItem.Object);
            repository.CreateUser(userName);
            repository.CreateTeam(teamName);
            repository.CreateActivityHistory(user.Object, team.Object, board.Object, workItem.Object);
            Assert.AreEqual(repository.Teams.Count, 1);
            Assert.AreEqual(repository.Users.Count, 1);
            Assert.AreEqual(repository.ActivityHistories.Count, 1);
            Assert.AreEqual(repository.WorkItems.Count, 1);
            Assert.AreEqual(repository.UserExists(userName), true);
            Assert.AreEqual(repository.TeamExists(teamName), true);
            Assert.AreEqual(repository.GetTeamByName(teamName).Name, team.Object.Name);
            Assert.AreEqual(repository.GetUserByName(userName).Name, user.Object.Name);
            Assert.AreEqual(repository.ShowTeamActivity(teamName)[0].Team.Name, teamName);
            Assert.AreEqual(repository.ShowUserActivity(userName)[0].User.Name, userName);

        }
    }
}
