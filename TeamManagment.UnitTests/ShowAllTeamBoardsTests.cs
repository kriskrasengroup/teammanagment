﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using TaskManagement.Commands;
using TaskManagement.Contracts;
using TaskManagement.Core.Contracts;
using TaskManagement.Models;

namespace TeamManagment.UnitTests
{
    [TestClass]
    public class ShowAllTeamBoardsTests
    {
        [TestMethod]
        public void ShouldThrowAll_TeamBoardsExists()
        {
            var teamName = "SuicideSquad";
            var commandParameters = new List<string> { teamName };
            var repository = new Mock<IRepository>();
            var team = new Mock<ITeam>();
            team.SetupGet(x => x.Name).Returns(teamName);
            repository.Setup(x => x.Teams).Returns(new List<ITeam> { team.Object });
            repository.Setup(x => x.TeamExists(teamName)).Returns(false);
            var test = new ShowAllTeamBoardsCommand(commandParameters, repository.Object);
            Assert.ThrowsException<ArgumentException>(() => test.Execute());
        }

        [TestMethod]
        public void ShouldAll_ParametersLess_ThenExpected()
        {            
            var repository = new Mock<IRepository>();
            var commandParameters = new List<string>();

            var test = new ShowAllTeamBoardsCommand(commandParameters, repository.Object);
            Assert.ThrowsException<ArgumentException>(() => test.Execute());
        }


        [TestMethod]
        public void ShouldAll_Board_ExpectedPrinted()
        {
            var teamTest = "TeamKillers";
            var testBoard = "TestBoard";

            var repository = new Mock<IRepository>();
            var commandParameters = new List<string> { teamTest, testBoard };

            var team = new Mock<ITeam>();
            team.SetupGet(x => x.Name).Returns(teamTest);
            var board = new Mock<IBoard>();
            board.SetupGet(x => x.Name).Returns(teamTest);
            team.SetupGet(x => x.Boards).Returns(new List<IBoard> { board.Object });
            repository.SetupGet(x => x.Teams).Returns(new List<ITeam> { team.Object });
            repository.Setup(x => x.TeamExists(teamTest)).Returns(true);
            var test = new ShowAllTeamBoardsCommand(commandParameters, repository.Object);
            var sb = new StringBuilder();
            sb.AppendLine($"===ShowAllBoardsOfTeam: {teamTest}===");

            foreach (var item in team.Object.Boards)
            {
                sb.AppendLine($"boards: {item.Name}");
            }
            sb.AppendLine($" ----------  ");           
            var expected = sb.ToString().Trim();
            var actual = test.Execute();
            Assert.AreEqual(expected, actual);
        }
    }
}
