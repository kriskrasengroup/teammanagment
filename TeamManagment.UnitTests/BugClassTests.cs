﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using TaskManagement.Contracts;
using TaskManagement.Enums;
using TaskManagement.Models;

namespace TeamManagment.UnitTests
{
    [TestClass]
    public class BugClassTests
    {
        [TestMethod]
        public void PassValues_Should()
        {
            //Arrange
            var id = 1;
            var title = "Test Titleee";
            var description = "Test Description";
            var user = new Mock<IUser>();
            var stepsToProduce = new List<string> { "TestProduce" };
            user.SetupGet(x => x.Name).Returns("Kriskoo");


            //Act
            var bug = new Bug(id, title, description, stepsToProduce, Priority.HIGH, Severity.CRITICAL, BugStatus.ACTIVE);
            bug.Assignee = user.Object;
            bug.AddComment("TestComment", "TestUser");
            var comment = new Mock<IComment>();
            comment.SetupGet(x => x.Author).Returns("TestUser");
            comment.SetupGet(x => x.Content).Returns("TestComment");

            //Assert
            Assert.AreEqual(bug.Title, "Test Titleee");
            Assert.AreEqual(bug.Description, "Test Description");
            Assert.AreEqual(bug.Priority, Priority.HIGH);
            Assert.AreEqual(bug.Severity, Severity.CRITICAL);
            Assert.AreEqual(bug.Status, BugStatus.ACTIVE);
            Assert.AreEqual("Kriskoo", bug.Assignee.Name);
            Assert.AreEqual(bug.StepsToReproduce[0], "TestProduce");
            Assert.AreEqual(1, bug.Id);
            Assert.AreEqual(bug.Comment[0].Author, comment.Object.Author);
            Assert.AreEqual(bug.Comment[0].Content, comment.Object.Content);
        }

        [TestMethod]
        public void Constructor_ShouldCreate_WhenValueCorrect()
        {
            var name = "TestUser";
            var user = new Board(name);
            var workItem = new Mock<IWorkItem>();
            user.AddWorkItem(workItem.Object);
            var expected = new List<IWorkItem> { workItem.Object };
            var actual = user.WorkItems;

            Assert.AreEqual(actual.Count, expected.Count);
        }
    }
}
