﻿using System;
using System.Collections.Generic;
using TaskManagement.Common;
using TaskManagement.Core.Contracts;

namespace TaskManagement.Commands.Adding
{
    public class AddUserToTeamCommand : BaseCommand
    {
        private readonly IRepository repository;
        private readonly IList<string> commandParameters;

        public AddUserToTeamCommand(IList<string> commandParameters, IRepository repository)
            : base(commandParameters, repository)
        {
            this.repository = repository;
            this.commandParameters = commandParameters;
        }

        public override string Execute()
        {
            if (commandParameters.Count < 2)
            {
                throw new ArgumentException($"Provide UserName and TeamName");
            }

            Validator.ValidateArgumentIsNotNull(this.commandParameters, Constants.ADD_NULL_ERR);
            Validator.CreateMinParameters(this.commandParameters.Count, Constants.ADD_USER_MIN_PARAMETERS, string.Format(Constants.ADD_USER_LEN_ERR, Constants.ADD_USER_MIN_PARAMETERS, this.CommandParameters.Count));

            string userName = this.commandParameters[0];
            string teamName = this.commandParameters[1];

            if (!repository.UserExists(userName))
            {
                throw new ArgumentException($"User {userName} does not exist.");
            }

            if (!repository.TeamExists(teamName))
            {
                throw new ArgumentException($"Team {teamName} does not exist");
            }

            var user = this.repository.GetUserByName(userName);
            var team = this.repository.GetTeamByName(teamName);

            team.AddUser(user);

            return $"User {userName} was added to Team {teamName}.";
        }
    }
}
