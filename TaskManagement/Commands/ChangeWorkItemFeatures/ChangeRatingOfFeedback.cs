﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaskManagement.Common;
using TaskManagement.Contracts;
using TaskManagement.Core.Contracts;

namespace TaskManagement.Commands
{
    public class ChangeRatingOfFeedback : BaseCommand
    {
        private readonly IList<string> commandParameters;
        private readonly IRepository repository;
        public ChangeRatingOfFeedback(IList<string> commandParameters, IRepository repository)
            : base(repository)
        {
            this.commandParameters = commandParameters;
            this.repository = repository;
        }
        public override string Execute()
        {
            if (commandParameters.Count < 2)
            {
                throw new ArgumentException($"Provide Title and Rating.");
            }
            Validator.ValidateArgumentIsNotNull(commandParameters, Constants.TITLE_NULL_ERR);
            Validator.ValidateIntRange(commandParameters[0].Length, Constants.TITLE_NAME_LEN_MIN, Constants.TITLE_NAME_LEN_MAX, Constants.TITLE_NAME_LEN_ERR);

            var feedBack = this.commandParameters[0];
            int rating = int.Parse(this.commandParameters[1]);

            var workItemExists = this.repository.WorkItems.Any(x => x.Title.ToLower() == feedBack.ToLower());

            if (!workItemExists)
            {
                throw new ArgumentException($"WorkItem with title: {feedBack} does not exists.");
            }

            var item = (IFeedback)this.repository.WorkItems.FirstOrDefault(x => x.Title.ToLower() == feedBack.ToLower());
            var previosRating = item.Rating;
            item.Rating = rating;

            return $"Changed rating of Feedback {item.Title} with ID: {item.Id} from {previosRating} to {rating}";
        }
    }
}
