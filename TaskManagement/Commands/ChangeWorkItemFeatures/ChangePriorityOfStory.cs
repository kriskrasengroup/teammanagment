﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaskManagement.Common;
using TaskManagement.Contracts;
using TaskManagement.Core.Contracts;
using TaskManagement.Enums;

namespace TaskManagement.Commands
{
    public class ChangePriorityOfStory : BaseCommand
    {
        private readonly IList<string> commandParameters;
        private readonly IRepository repository;
        public ChangePriorityOfStory(IList<string> commandParameters, IRepository repository)
            : base(repository)
        {
            this.commandParameters = commandParameters;
            this.repository = repository;
        }

        public override string Execute()
        {
            Validator.ValidateArgumentIsNotNull(commandParameters, Constants.TITLE_NULL_ERR);
            Validator.ValidateIntRange(commandParameters[0].Length, Constants.TITLE_NAME_LEN_MIN, Constants.TITLE_NAME_LEN_MAX, Constants.TITLE_NAME_LEN_ERR);

            var story = this.commandParameters[0];
            Priority priority = (Priority)Enum.Parse(typeof(Priority), this.commandParameters[1]);

            var workItemExists = this.repository.WorkItems.Any(x => x.Title.ToLower() == story.ToLower());

            if (!workItemExists)
            {
                throw new ArgumentException($"WorkItem with title: {story} does not exists.");
            }

            var item = (IStory)this.repository.WorkItems.FirstOrDefault(x => x.Title.ToLower() == story.ToLower());
            var previosPriority = item.Priority;
            item.Priority = priority;

            return $"Changed priority of Story {item.Title} with ID: {item.Id} from {previosPriority} to {priority}";
        }
    }
}
