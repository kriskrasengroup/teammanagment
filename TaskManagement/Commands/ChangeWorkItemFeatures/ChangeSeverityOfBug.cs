﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaskManagement.Contracts;
using TaskManagement.Core.Contracts;
using TaskManagement.Enums;

namespace TaskManagement.Commands
{
    public class ChangeSeverityOfBug : BaseCommand
    {
        private readonly IList<string> commandParameters;
        private readonly IRepository repository;
        public ChangeSeverityOfBug(IList<string> commandParameters, IRepository repository)
            : base(repository)
        {
            this.commandParameters = commandParameters;
            this.repository = repository;
        }
        public override string Execute()
        {
            var bug = this.commandParameters[0];
            Severity severity = (Severity)Enum.Parse(typeof(Severity), this.commandParameters[1]);

            var item = (IBug)this.repository.WorkItems.FirstOrDefault(x => x.Title.ToLower() == bug.ToLower());
            var workItemExists = this.repository.WorkItems.Any(x => x.Title.ToLower() == bug.ToLower());

            if (!workItemExists)
            {
                throw new ArgumentException($"WorkItem with title: {bug} does not exists.");
            }

            var previosSeverity = item.Severity;
            item.Severity = severity;

            return $"Changed severity of Bug {item.Title} with ID: {item.Id} from {previosSeverity} to {severity}";
        }
    }
}
