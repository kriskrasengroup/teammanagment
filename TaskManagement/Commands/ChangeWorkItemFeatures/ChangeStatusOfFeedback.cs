﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaskManagement.Contracts;
using TaskManagement.Core.Contracts;
using TaskManagement.Enums;

namespace TaskManagement.Commands
{
    public class ChangeStatusOfFeedback : BaseCommand
    {
        private readonly IList<string> commandParameters;
        private readonly IRepository repository;
        public ChangeStatusOfFeedback(IList<string> commandParameters, IRepository repository)
            : base(repository)
        {
            this.commandParameters = commandParameters;
            this.repository = repository;
        }
        public override string Execute()
        {
            if (this.commandParameters.Count < 1)
            {
                throw new ArgumentException($"Provide WorkItemName");
            }

            var feedback = this.commandParameters[0];
            FeedbackStatus status = (FeedbackStatus)Enum.Parse(typeof(FeedbackStatus), this.commandParameters[1]);

            var workItemExists = this.repository.WorkItems.Any(x => x.Title.ToLower() == feedback.ToLower());

            if (!workItemExists)
            {
                throw new ArgumentException($"WorkItem with title: {feedback} does not exists.");
            }

            var item = (IFeedback)this.repository.WorkItems.FirstOrDefault(x => x.Title.ToLower() == feedback.ToLower());
            var previousStatus = item.Status;
            item.Status = status;

            return $"Changed status of Feedback {item.Title} with ID: {item.Id} from {previousStatus} to {status}";
        }
    }
}
