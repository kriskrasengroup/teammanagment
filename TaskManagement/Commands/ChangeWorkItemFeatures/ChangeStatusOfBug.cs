﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaskManagement.Contracts;
using TaskManagement.Core.Contracts;
using TaskManagement.Enums;

namespace TaskManagement.Commands
{
    public class ChangeStatusOfBug : BaseCommand
    {
        private readonly IList<string> commandParameters;
        private readonly IRepository repository;
        public ChangeStatusOfBug(IList<string> commandParameters, IRepository repository)
            : base(repository)
        {
            this.commandParameters = commandParameters;
            this.repository = repository;
        }
        public override string Execute()
        {
            var bug = this.commandParameters[0];
            BugStatus status = (BugStatus)Enum.Parse(typeof(BugStatus), this.commandParameters[1]);

            var workItemExists = this.repository.WorkItems.Any(x => x.Title.ToLower() == bug.ToLower());

            if (!workItemExists)
            {
                throw new ArgumentException($"WorkItem with title: {bug} does not exists.");
            }

            var item = (IBug)this.repository.WorkItems.FirstOrDefault(x => x.Title.ToLower() == bug.ToLower());
            var previosStatus = item.Status;
            item.Status = status;

            return $"Changed status of Bug {item.Title} with ID: {item.Id} from {previosStatus} to {status}";
        }
    }
}
