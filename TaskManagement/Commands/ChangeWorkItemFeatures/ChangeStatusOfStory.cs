﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaskManagement.Contracts;
using TaskManagement.Core.Contracts;
using TaskManagement.Enums;

namespace TaskManagement.Commands
{
    public class ChangeStatusOfStory : BaseCommand
    {
        private readonly IList<string> commandParameters;
        private readonly IRepository repository;
        public ChangeStatusOfStory(IList<string> commandParameters, IRepository repository)
            : base(repository)
        {
            this.commandParameters = commandParameters;
            this.repository = repository;
        }

        public override string Execute()
        {
            if (this.commandParameters.Count < 1)
            {
                throw new ArgumentException("Provide WorkItem Name.");
            }

            var story = this.commandParameters[0];
            StoryStatus status = (StoryStatus)Enum.Parse(typeof(StoryStatus), this.commandParameters[1]);
            var workItemExists = this.repository.WorkItems.Any(x => x.Title.ToLower() == story.ToLower());

            if (!workItemExists)
            {
                throw new ArgumentException($"WorkItem with title: {story} does not exists.");
            }

            var item = (IStory)this.repository.WorkItems.FirstOrDefault(x => x.Title.ToLower() == story.ToLower());
            var previosStatus = item.Status;
            item.Status = status;

            return $"Changed status of Story {item.Title} with ID: {item.Id} from {previosStatus} to {status}";
        }
    }
}
