﻿using System.Collections.Generic;
using TaskManagement.Commands.Contracts;
using TaskManagement.Core.Contracts;

namespace TaskManagement.Commands
{
    public abstract class BaseCommand : ICommand
    {
        protected BaseCommand(IRepository repository)
            : this(new List<string>(), repository)
        {
        }

        protected BaseCommand(IList<string> commandParameters, IRepository repository)
        {
            this.CommandParameters = commandParameters;
            this.Repository = repository;
        }
        public abstract string Execute();

        protected IList<string> CommandParameters { get; }

        protected IRepository Repository { get; }
    }
}
