﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TaskManagement.Core.Contracts;

namespace TaskManagement.Commands
{
    public class ShowUserActivityCommand : BaseCommand
    {
        private readonly IRepository repository;
        private readonly IList<string> commandParameters;

        public ShowUserActivityCommand(IList<string> commandParameters, IRepository repository)
            : base(repository)
        {
            this.repository = repository;
            this.commandParameters = commandParameters;
        }
        public override string Execute()
        {
            if (this.commandParameters.Count < 1)
            {
                throw new ArgumentException("please provide username");
            }

            var username = commandParameters[0];

            if (!this.repository.UserExists(username))
            {
                throw new ArgumentException($"User with name {username} do not exist.");
            }

            var activity = this.repository.ActivityHistories.Where(x => x.User.Name == username).ToList();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"User: {username}");

            foreach (var item in activity)
            {
                sb.AppendLine($"Has WorkItem: {item.WorkItem.Title} with ID: {item.WorkItem.Id}");
                sb.AppendLine($"Description: {item.WorkItem.Description}");
                sb.AppendLine("  ----------  ");
            }
            return sb.ToString().TrimEnd();
        }
    }
}
