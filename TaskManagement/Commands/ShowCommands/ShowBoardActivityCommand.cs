﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TaskManagement.Core.Contracts;

namespace TaskManagement.Commands
{
    public class ShowBoardActivityCommand : BaseCommand
    {
        private readonly IRepository repository;
        private readonly IList<string> commandParameters;

        public ShowBoardActivityCommand(IList<string> commandParameters, IRepository repository)
            : base(repository)
        {
            this.repository = repository;
            this.commandParameters = commandParameters;
        }
        public override string Execute()
        {
            if (this.commandParameters.Count < 2)
            {
                throw new ArgumentException("please provide teamname and boardName");
            }

            var teamName = commandParameters[0];
            var board = commandParameters[1];

            if (!this.repository.TeamExists(teamName))
            {
                throw new ArgumentException($"Team with name {teamName} do not exist.");
            }
            var boardExists = BoardExists(teamName, board);


            if (!boardExists)
            {
                throw new ArgumentException($"Board with name {board} do not exist.");
            }

            var boardActivity = this.repository
                .ActivityHistories.FirstOrDefault(x => x.Team.Name.ToLower() == teamName.ToLower() && x.Board.Name.ToLower() == board.ToLower())
                .Board.WorkItems.ToList();

            var sb = new StringBuilder();
            sb.AppendLine($"Board: {board} in team: {teamName} has following workItems.");

            foreach (var item in boardActivity)
            {
                sb.AppendLine($"Title: {item.Title}");
                sb.AppendLine($"Description: {item.Description}");
                sb.AppendLine("  ---------  ");
            }
            return sb.ToString().TrimEnd();
        }
        public bool BoardExists(string teamName, string board)
        {
            return this.repository.Teams.FirstOrDefault(x => x.Name.ToLower() == teamName.ToLower())
                .Boards.Any(x => x.Name.ToLower() == board.ToLower());
        }
    }
}
