﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TaskManagement.Core.Contracts;

namespace TaskManagement.Commands
{
    public class ShowAllTeamMembersCommand : BaseCommand
    {
        private readonly IRepository repository;
        private readonly IList<string> commandParameters;
        public ShowAllTeamMembersCommand(IList<string> commandParameters, IRepository repository)
            : base(commandParameters, repository)
        {
            this.repository = repository;
            this.commandParameters = commandParameters;
        }

        public override string Execute()
        {
            if (this.commandParameters.Count < 1)
            {
                throw new ArgumentException("please provide username");
            }

            var teamName = this.commandParameters[0];
            if (!repository.UserExists(teamName))
            {
                throw new ArgumentException($"User with name {teamName} do not exist.");
            }
            var team = repository.Teams.FirstOrDefault(x => x.Name.ToLower() == teamName.ToLower());

            var sb = new StringBuilder();
            sb.AppendLine($"===ShowAllMembersOfTeam: {teamName}===");

            foreach (var item in team.Users)
            {
                sb.AppendLine($"members: {item.Name}");
            }
            sb.AppendLine($" ----------  ");

            return sb.ToString().Trim();
        }
    }
}
