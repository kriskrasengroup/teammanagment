﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TaskManagement.Contracts;
using TaskManagement.Core.Contracts;

namespace TaskManagement.Commands.ShowCommands
{
    public class ShowWorkItemByTitle : BaseCommand
    {
        private IList<string> commandParameters;
        private IRepository repository;

        public ShowWorkItemByTitle(IList<string> commandParameters, IRepository repository)
            : base(repository)
        {
            this.commandParameters = commandParameters;
            this.repository = repository;
        }
        public override string Execute()
        {
            if (commandParameters.Count < 1)
            {
                throw new ArgumentException($"Provide order, Ascending or Descending.");
            }

            var order = commandParameters[0].ToLower();
            //var filter = commandParameters[1];
            var items = new List<IWorkItem>();

            if (order == "ascending")
            {
                items = this.repository.WorkItems.OrderBy(x => x.Title).ToList();
            }

            else if (order == "descending")
            {
                items = this.repository.WorkItems.OrderByDescending(x => x.Title).ToList();
            }

            var sb = new StringBuilder();
            foreach (var item in items)
            {
                sb.AppendLine(item.Title);
            }
            return sb.ToString();
        }
    }
}
