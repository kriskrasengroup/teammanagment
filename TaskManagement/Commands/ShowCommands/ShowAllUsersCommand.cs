﻿using System.Collections.Generic;
using System.Text;
using TaskManagement.Core.Contracts;

namespace TaskManagement.Commands
{
    public class ShowAllUsersCommand : BaseCommand
    {
        private readonly IRepository repository;
        private readonly IList<string> commandParameters;

        public ShowAllUsersCommand(IList<string> commandParameters, IRepository repository)
            : base(commandParameters, repository)
        {
            this.commandParameters = commandParameters;
            this.repository = repository;
        }

        public override string Execute()
        {
            var sb = new StringBuilder();
            sb.AppendLine("===ShowAllUsers===");
            foreach (var item in this.Repository.Users)
            {
                sb.AppendLine($"User: {item.Name}");
            }

            return sb.ToString().Trim();
        }
    }
}
