﻿using System.Collections.Generic;
using System.Text;
using TaskManagement.Core.Contracts;

namespace TaskManagement.Commands
{
    public class ShowAllTeamsCommand : BaseCommand
    {
        private readonly IRepository repository;
        private readonly IList<string> commandParameters;

        public ShowAllTeamsCommand(IList<string> commandParameters, IRepository repository)
            : base(commandParameters, repository)
        {
            this.commandParameters = commandParameters;
            this.repository = repository;
        }

        public override string Execute()
        {
            var sb = new StringBuilder();
            sb.AppendLine("===All Teams===");
            foreach (var item in this.repository.Teams)
            {
                sb.AppendLine($"Team: {item.Name}");
            }

            return sb.ToString().Trim();
        }
    }
}
