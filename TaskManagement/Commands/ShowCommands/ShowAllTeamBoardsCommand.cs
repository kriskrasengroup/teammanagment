﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TaskManagement.Core.Contracts;

namespace TaskManagement.Commands
{
    public class ShowAllTeamBoardsCommand : BaseCommand
    {
        private readonly IRepository repository;
        private readonly IList<string> commandParameters;

        public ShowAllTeamBoardsCommand(List<string> commandParameters, IRepository repository)
            : base(commandParameters, repository)
        {
            this.repository = repository;
            this.commandParameters = commandParameters;
        }

        public override string Execute()
        {
            if (this.commandParameters.Count < 1)
            {
                throw new ArgumentException("please provide teamName");
            }

            var teamName = this.commandParameters[0];
            if (!repository.TeamExists(teamName))
            {
                throw new ArgumentException($"Team with name {teamName} do not exist.");
            }
            var team = repository.Teams.FirstOrDefault(x => x.Name.ToLower() == teamName.ToLower());

            var sb = new StringBuilder();
            sb.AppendLine($"===ShowAllBoardsOfTeam: {teamName}===");

            foreach (var item in team.Boards)
            {
                sb.AppendLine($"boards: {item.Name}");
            }
            sb.AppendLine($" ----------  ");

            return sb.ToString().Trim();
        }
    }
}
