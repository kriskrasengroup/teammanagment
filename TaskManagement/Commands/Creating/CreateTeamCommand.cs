﻿using System;
using System.Collections.Generic;
using TaskManagement.Common;
using TaskManagement.Core.Contracts;

namespace TaskManagement.Commands.Creating
{
    public class CreateTeamCommand : BaseCommand
    {
        private readonly IRepository repository;
        private readonly IList<string> commandParameters;

        public CreateTeamCommand(IList<string> commandParameters, IRepository repository)
            : base(commandParameters, repository)
        {
            this.repository = repository;
            this.commandParameters = commandParameters;
        }

        public override string Execute()
        {
            Validator.ValidateArgumentIsNotNull(this.CommandParameters, Constants.CREATE_TEAM_NULL_ERR);
            Validator.CreateMinParameters(this.CommandParameters.Count, Constants.CREATE_TEAM_MIN_PARAMETERS, string.Format(Constants.CREATE_TEAM_LEN_ERR, Constants.CREATE_TEAM_MIN_PARAMETERS, this.CommandParameters.Count));

            string team = this.CommandParameters[0];

            if (this.Repository.TeamExists(team))
            {
                throw new ArgumentException($"A team with the team name {team} already exists!");

            }

            this.Repository.CreateTeam(team);

            return $"Succesfully Created Team with name {this.CommandParameters[0]}";
        }
    }
}
