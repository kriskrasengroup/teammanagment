﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaskManagement.Core.Contracts;
using TaskManagement.Enums;
using TaskManagement.Models;

namespace TaskManagement.Commands.Creating
{
    public class CreateStoryOnBoard : BaseCommand
    {
        private readonly IRepository repository;
        private readonly IList<string> commandParameters;
        private readonly int id;
        public CreateStoryOnBoard(int id, IList<string> commandParameters, IRepository repository)
            : base(commandParameters, repository)
        {
            this.repository = repository;
            this.commandParameters = commandParameters;
            this.id = id;
        }

        public override string Execute()
        {
            if (this.commandParameters.Count < 8)
            {
                throw new ArgumentException("please provide teamName,boardName,title,description,priority,size and status");
            }
            string userName = this.commandParameters[0];
            string teamName = this.commandParameters[1];
            string boardName = this.commandParameters[2];
            string title = this.CommandParameters[3];
            string description = this.CommandParameters[4];

            Priority priority = (Priority)Enum.Parse(typeof(Priority), this.commandParameters[5].ToUpper());
            Size size = (Size)Enum.Parse(typeof(Size), this.commandParameters[6].ToUpper());
            StoryStatus storyStatus = (StoryStatus)Enum.Parse(typeof(StoryStatus), this.commandParameters[7].ToUpper());

            if (!this.repository.TeamExists(teamName))
            {
                throw new ArgumentException($"Team with name {teamName} do not exist.");
            }

            var boardExists = this.repository.Teams.FirstOrDefault(x => x.Name.ToLower() == teamName.ToLower())
                .Boards.Any(x => x.Name.ToLower() == boardName.ToLower());

            if (!boardExists)
            {
                throw new ArgumentException($"board with name {boardName} do not exist.");
            }

            var story = new Story(id, title, description, priority, size, storyStatus);
            story.Assignee = repository.GetUserByName(userName);

            repository.AddWorkItem(story);
            var user = this.repository.GetUserByName(userName);
            user.AddWorkItem(story);

            var team = this.repository.GetTeamByName(teamName);
            var boardToAddTo = this.repository.Teams.FirstOrDefault(x => x.Name.ToLower() == teamName.ToLower())
                .Boards.FirstOrDefault(x => x.Name.ToLower() == boardName.ToLower());

            boardToAddTo.AddWorkItem(story);
            repository.CreateActivityHistory(user, team, boardToAddTo, story);

            return $"Created story: {story.Title} on board {boardName} in {teamName}";
        }
    }
}