﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaskManagement.Core.Contracts;
using TaskManagement.Enums;
using TaskManagement.Models;

namespace TaskManagement.Commands.Creating
{
    public class CreateFeedbackOnBoard : BaseCommand
    {
        private readonly IList<string> commandParameters;
        private readonly IRepository repository;
        private readonly int id;

        public CreateFeedbackOnBoard(int id, IList<string> commandParameters, IRepository repository)
            : base(commandParameters, repository)
        {
            this.repository = repository;
            this.commandParameters = commandParameters;
            this.id = id;
        }

        public override string Execute()
        {
            if (this.commandParameters.Count < 6)
            {
                throw new ArgumentException("please provide teamName, boardName, title, description, rating, status");
            }

            string userName = this.commandParameters[0];
            string teamName = this.commandParameters[1];
            string boardName = this.commandParameters[2];
            string title = this.CommandParameters[3];
            string description = this.CommandParameters[4];
            int rating = int.Parse(this.commandParameters[5]);

            FeedbackStatus feedbackStatus = (FeedbackStatus)Enum.Parse(typeof(FeedbackStatus), this.commandParameters[6].ToUpper());

            var feedback = new Feedback(id, title, description, rating, feedbackStatus);

            if (!this.repository.TeamExists(teamName))
            {
                throw new ArgumentException($"Team with name {teamName} do not exist.");
            }

            var boardExists = this.repository.Teams.FirstOrDefault(x => x.Name.ToLower() == teamName.ToLower())
                .Boards.Any(x => x.Name.ToLower() == boardName.ToLower());

            if (!boardExists)
            {
                throw new ArgumentException($"Board with name {boardName} do not exist.");
            }

            var user = repository.GetUserByName(userName);
            user.AddWorkItem(feedback);

            var team = repository.GetTeamByName(teamName);
            repository.AddWorkItem(feedback);

            var boardToAddTo = this.repository.Teams.FirstOrDefault(x => x.Name.ToLower() == teamName.ToLower())
                .Boards.FirstOrDefault(x => x.Name.ToLower() == boardName.ToLower());

            boardToAddTo.AddWorkItem(feedback);
            repository.CreateActivityHistory(user, team, boardToAddTo, feedback);

            return $"Created feedback: {feedback.Title} on board {boardName} in {teamName}";
        }
    }
}