﻿using System;
using System.Collections.Generic;
using TaskManagement.Common;
using TaskManagement.Core.Contracts;

namespace TaskManagement.Commands.Creating
{
    public class CreateUserCommand : BaseCommand
    {
        private readonly IRepository repository;
        private readonly IList<string> commandParameters;

        public CreateUserCommand(IList<string> commandParameters, IRepository repository)
            : base(commandParameters, repository)
        {
            this.repository = repository;
            this.commandParameters = commandParameters;
        }

        public override string Execute()
        {
            Validator.ValidateArgumentIsNotNull(this.CommandParameters, Constants.CREATE_NULL_ERR);
            Validator.CreateMinParameters(this.CommandParameters.Count, Constants.CREATE_USER_MIN_PARAMETERS, string.Format(Constants.CREATE_USER_LEN_ERR, Constants.CREATE_USER_MIN_PARAMETERS, this.CommandParameters.Count));

            string username = this.CommandParameters[0];

            if (this.Repository.UserExists(username))
            {
                throw new ArgumentException($"A user with the username {username} already exists!");

            }

            this.Repository.CreateUser(username);

            return $"Succesfully Created User with name {this.CommandParameters[0]}";
        }
    }
}

