﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaskManagement.Core.Contracts;
using TaskManagement.Models;

namespace TaskManagement.Commands
{
    public class CreateBoardInTeamCommand : BaseCommand
    {
        private readonly IRepository repository;
        private readonly IList<string> commandParameters;
        public CreateBoardInTeamCommand(IList<string> commandParameters, IRepository repository)
            : base(commandParameters, repository)
        {
            this.repository = repository;
            this.commandParameters = commandParameters;
        }

        public override string Execute()
        {
            if (this.commandParameters.Count < 2)
            {
                throw new ArgumentException("please provide team");
            }

            string team = this.commandParameters[0];

            if (!this.repository.TeamExists(team))
            {
                throw new ArgumentException($"A team with the team name {team} does not exist!");
            }

            var board = new Board(this.commandParameters[1]);
            var hasBoard = this.repository.Teams.FirstOrDefault(x => x.Name.ToLower() == team.ToLower()).Boards.Any(x => x.Name.ToString().ToLower() != board.Name.ToLower());
            if (hasBoard)
            {
                throw new ArgumentException($"A board with name {board} already exists!");
            }

            this.repository.GetTeamByName(team).AddBoard(board);

            return $"Succesfully Created Board with name {board.Name} in team {team}";
        }
    }
}
