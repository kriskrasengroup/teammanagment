﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaskManagement.Common;
using TaskManagement.Core.Contracts;
using TaskManagement.Enums;
using TaskManagement.Models;

namespace TaskManagement.Commands.Creating
{
    public class CreateBugOnBoard : BaseCommand
    {
        private readonly IList<string> commandParameters;
        private readonly IRepository repository;
        private readonly IConsoleActions read;
        private readonly int id;

        public CreateBugOnBoard(int id, IList<string> commandParameters, IRepository repository, IConsoleActions read)
            : base(commandParameters, repository)
        {
            this.repository = repository;
            this.read = read;
            this.commandParameters = commandParameters;
            this.id = id;
        }

        public override string Execute()
        {
            if (this.commandParameters.Count < 8)
            {
                throw new ArgumentException("please provide teamName, boardName, title, description, priority, severity, bugStatus.");
            }

            string userName = this.commandParameters[0];
            string teamName = this.commandParameters[1];
            string boardName = this.commandParameters[2];
            string title = this.CommandParameters[3];
            string description = this.CommandParameters[4];
            this.read.Write("Steps to reproduce: ");

            List<string> stepsToProduce = this.read.ReadingLine().Split(' ').ToList();
            Priority priority = (Priority)Enum.Parse(typeof(Priority), this.commandParameters[5].ToUpper());
            Severity severity = (Severity)Enum.Parse(typeof(Severity), this.commandParameters[6].ToUpper());
            BugStatus bugStatus = (BugStatus)Enum.Parse(typeof(BugStatus), this.commandParameters[7].ToUpper());

            if (!this.repository.TeamExists(teamName))
            {
                throw new ArgumentException($"Team with name {teamName} do not exist.");
            }
            var boardExists = this.repository.Teams.FirstOrDefault(x => x.Name.ToLower() == teamName.ToLower())
                .Boards.Any(x => x.Name.ToLower() == boardName.ToLower());

            if (!boardExists)
            {
                throw new ArgumentException($"board with name {boardName} do not exist.");
            }

            var bug = new Bug(id, title, description, stepsToProduce, priority, severity, bugStatus);

            var user = repository.GetUserByName(userName);
            user.AddWorkItem(bug);
            var team = repository.GetTeamByName(teamName);

            repository.AddWorkItem(bug);
            bug.Assignee = user;

            var boardToAddTo = this.repository.Teams.FirstOrDefault(x => x.Name.ToLower() == teamName.ToLower())
                .Boards.FirstOrDefault(x => x.Name.ToLower() == boardName.ToLower());

            boardToAddTo.AddWorkItem(bug);
            repository.CreateActivityHistory(user, team, boardToAddTo, bug);

            return $"Created bug: {bug.Title} on board {boardName} in {teamName}";
        }
    }
}
