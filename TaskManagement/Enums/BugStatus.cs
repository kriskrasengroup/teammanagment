﻿namespace TaskManagement.Enums
{
    public enum BugStatus
    {
        ACTIVE,
        FIXED
    }
}
