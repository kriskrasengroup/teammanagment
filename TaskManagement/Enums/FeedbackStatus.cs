﻿namespace TaskManagement.Enums
{
    public enum FeedbackStatus
    {
        NEW,
        UNSCHEDULED,
        SCHEDULED,
        DONE
    }
}
