﻿namespace TaskManagement.Enums
{
    public enum Size
    {
        LARGE,
        MEDIUM,
        SMALL
    }
}
