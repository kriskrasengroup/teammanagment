﻿namespace TaskManagement.Enums
{
    public enum Severity
    {
        CRITICAL,
        MAJOR,
        MINOR
    }
}
