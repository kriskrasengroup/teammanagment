﻿namespace TaskManagement.Enums
{
    public enum Priority
    {
        HIGH,
        MEDIUM,
        LOW
    }
}
