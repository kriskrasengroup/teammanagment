﻿namespace TaskManagement.Enums
{
    public enum StoryStatus
    {
        NOTDONE,
        INPROGRESS,
        DONE
    }
}
