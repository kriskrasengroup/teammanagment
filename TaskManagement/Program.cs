﻿using TaskManagement.Common;
using TaskManagement.Core;

namespace TaskManagement
{
    class Program
    {
        static void Main(string[] args)
        {
            var repository = new Repository();
            var read = new ReadingConsole();
            var commandFactory = new CommandFactory(repository, read);
            var engine = new Engine(commandFactory);
            engine.Start();
        }
    }
}
