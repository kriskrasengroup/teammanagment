﻿using System.Collections.Generic;
using System.Linq;
using TaskManagement.Contracts;
using TaskManagement.Core.Contracts;
using TaskManagement.Models;

namespace TaskManagement.Core
{
    public class Repository : IRepository
    {
        private readonly IList<ITeam> teams = new List<ITeam>();
        private readonly IList<IUser> users = new List<IUser>();
        private readonly IList<IWorkItem> workItems = new List<IWorkItem>();
        private readonly IList<IActivityHistory> activityHistories = new List<IActivityHistory>();

        public IList<ITeam> Teams
        {
            get
            {
                var copy = new List<ITeam>(this.teams);
                return copy;
            }
        }

        public IList<IUser> Users
        {
            get
            {
                var copy = new List<IUser>(this.users);
                return copy;
            }
        }

        public IList<IWorkItem> WorkItems
        {
            get
            {
                var copy = new List<IWorkItem>(this.workItems);
                return copy;
            }
        }

        public IList<IActivityHistory> ActivityHistories
        {
            get
            {
                var copy = new List<IActivityHistory>(this.activityHistories);
                return copy;
            }
        }

        public void AddWorkItem(IWorkItem workItem)
        {
            this.workItems.Add(workItem);
        }

        public IActivityHistory CreateActivityHistory(IUser user, ITeam team, IBoard board, IWorkItem workItem)
        {
            var activityHistory = new ActivityHistory(user, team, board, workItem);
            this.activityHistories.Add(activityHistory);
            return activityHistory;
        }

        public ITeam CreateTeam(string teamName)
        {
            var team = new Team(teamName);
            this.teams.Add(team);
            return team;
        }

        public IUser CreateUser(string userName)
        {
            var user = new User(userName);
            this.users.Add(user);
            return user;
        }

        public ITeam GetTeamByName(string teamName)
        {
            var team = this.teams.Single(x => x.Name.ToLower() == teamName.ToLower());
            return team;
        }

        public IUser GetUserByName(string userName)
        {
            var user = this.users.Single(x => x.Name.ToLower() == userName.ToLower());
            return user;
        }

        public IList<IActivityHistory> ShowTeamActivity(string teamName)
        {
            var history = this.activityHistories.Where(x => x.Team.Name.ToLower() == teamName.ToLower()).ToList();
            return history;
        }

        public IList<IActivityHistory> ShowUserActivity(string userName)
        {
            var history = this.activityHistories.Where(x => x.User.Name.ToLower() == userName.ToLower()).ToList();
            return history;
        }

        public bool TeamExists(string teamName)
        {
            return teams.Any(x => x.Name.ToLower() == teamName.ToLower());
        }

        public bool UserExists(string userName)
        {
            return users.Any(x => x.Name.ToLower() == userName.ToLower());
        }
    }
}
