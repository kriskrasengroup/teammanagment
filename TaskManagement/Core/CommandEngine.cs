﻿using System;
using System.Collections.Generic;
using TaskManagement.Commands;
using TaskManagement.Commands.Adding;
using TaskManagement.Commands.Contracts;
using TaskManagement.Commands.Creating;
using TaskManagement.Commands.ShowCommands;
using TaskManagement.Common;
using TaskManagement.Core.Contracts;

namespace TaskManagement.Core
{
    public class CommandFactory : ICommandFactory
    {
        private readonly IRepository repository;
        private readonly IConsoleActions read;
        private int id;

        public CommandFactory(IRepository repository, IConsoleActions read)
        {
            this.repository = repository;
            this.read = read;
            this.id = 0;
        }

        public ICommand Create(string commandLine)
        {
            string[] arguments = commandLine.Split(" ", StringSplitOptions.RemoveEmptyEntries);

            string commandName = this.ExtractCommandName(arguments);
            List<string> commandParameters = this.ExtractCommandParameters(arguments);

            ICommand command;
            switch (commandName.ToLower())
            {
                case "createuser":
                    {
                        command = new CreateUserCommand(commandParameters, repository);
                        break;
                    }
                case "createteam":
                    {
                        command = new CreateTeamCommand(commandParameters, repository);
                        break;
                    }
                case "createstory":
                    {
                        command = new CreateStoryOnBoard(++id, commandParameters, repository);
                        break;
                    }
                case "createfeedback":
                    {
                        command = new CreateFeedbackOnBoard(++id, commandParameters, repository);
                        break;
                    }
                case "createbug":
                    {
                        command = new CreateBugOnBoard(++id, commandParameters, repository, read);
                        break;
                    }
                case "showuseractivity":
                    {
                        command = new ShowUserActivityCommand(commandParameters, repository);
                        break;
                    }
                case "showteamactivity":
                    {
                        command = new ShowTeamActivityCommand(commandParameters, repository);
                        break;
                    }
                case "showboardactivity":
                    {
                        command = new ShowBoardActivityCommand(commandParameters, repository);
                        break;
                    }
                case "showallusers":
                    {
                        command = new ShowAllUsersCommand(commandParameters, repository);
                        break;
                    }
                case "showallteammembers":
                    {
                        command = new ShowAllTeamMembersCommand(commandParameters, repository);
                        break;
                    }
                case "showallteamboards":
                    {
                        command = new ShowAllTeamBoardsCommand(commandParameters, repository);
                        break;
                    }
                case "listallteams":
                    {
                        command = new ShowAllTeamsCommand(commandParameters, repository);
                        break;
                    }
                case "changestatusofstory":
                    {
                        command = new ChangeStatusOfStory(commandParameters, repository);
                        break;
                    }
                case "changestatusoffeedback":
                    {
                        command = new ChangeStatusOfFeedback(commandParameters, repository);
                        break;
                    }
                case "changestatusofbug":
                    {
                        command = new ChangeStatusOfBug(commandParameters, repository);
                        break;
                    }
                case "changesizeofstory":
                    {
                        command = new ChangeSizeOfStory(commandParameters, repository);
                        break;
                    }
                case "changeseverityofbug":
                    {
                        command = new ChangeSeverityOfBug(commandParameters, repository);
                        break;
                    }
                case "changeratingoffeedback":
                    {
                        command = new ChangeRatingOfFeedback(commandParameters, repository);
                        break;
                    }
                case "changepriorityofstory":
                    {
                        command = new ChangePriorityOfStory(commandParameters, repository);
                        break;
                    }
                case "changepriorityofbug":
                    {
                        command = new ChangePriorityOfBug(commandParameters, repository);
                        break;
                    }
                case "createboardinteam":
                    {
                        command = new CreateBoardInTeamCommand(commandParameters, repository);
                        break;
                    }
                case "addusertoteam":
                    {
                        command = new AddUserToTeamCommand(commandParameters, repository);
                        break;
                    }
                case "addcommenttotask":
                    {
                        command = new AddCommentToTaskCommand(commandParameters, repository);
                        break;
                    }
                case "showworkitembytitle":
                    {
                        command = new ShowWorkItemByTitle(commandParameters, repository);
                        break;
                    }
                default:
                    throw new InvalidOperationException($"Command with name: {commandName} doesn't exist!");
            }
            return command;
        }

        // Receives a full line and extracts the command to be executed from it.
        // For example, if the input line is "FilterBy Assignee John", the method will return "FilterBy".
        private string ExtractCommandName(string[] arguments)
        {
            string commandName = arguments[0];
            return commandName;
        }

        // Receives a full line and extracts the parameters that are needed for the command to execute.
        // For example, if the input line is "FilterBy Assignee John",
        // the method will return a list of ["Assignee", "John"].
        private List<string> ExtractCommandParameters(string[] arguments)
        {
            List<string> commandParameters = new();

            for (int i = 1; i < arguments.Length; i++)
            {
                commandParameters.Add(arguments[i]);
            }

            return commandParameters;
        }
    }
}
