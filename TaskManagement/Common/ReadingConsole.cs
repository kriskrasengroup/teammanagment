﻿using System;

namespace TaskManagement.Common
{
    public class ReadingConsole : IConsoleActions
    {
        public string ReadingLine()
        {
            return Console.ReadLine();
        }

        public void Write(string input)
        {
            Console.Write(input);
        }
    }
}
