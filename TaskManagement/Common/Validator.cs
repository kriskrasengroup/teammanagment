﻿using System;

namespace TaskManagement.Common
{
    public class Validator
    {
        public static void ValidateIntRange(int value, int min, int max, string message)
        {
            if (value < min || value > max)
            {
                throw new ArgumentException(message);
            }
        }

        public static void ValidateArgumentIsNotNull(object arg, string message)
        {
            if (arg == null)
            {
                throw new ArgumentException(message);
            }
        }

        public static void CreateMinParameters(int value, int min, string message)
        {
            if (value < min)
            {
                throw new ArgumentException(message);
            }
        }
    }
}
