﻿using System.Collections.Generic;
using TaskManagement.Contracts;
using TaskManagement.Enums;

namespace TaskManagement.Models
{
    public class Bug : WorkItem, IBug
    {
        private IList<string> stepsToReproduce = new List<string>();
        private Priority priority;
        private Severity severity;
        private BugStatus status;
        private IUser assignee;

        public Bug(int id, string title, string description, IList<string> stepsToReproduce, Priority priority, Severity severity, BugStatus bugStatus)
            : base(title, id, description)
        {
            this.Priority = priority;
            this.Severity = severity;
            this.Status = bugStatus;
            this.StepsToReproduce = stepsToReproduce;
        }

        public IList<string> StepsToReproduce
        {
            get
            {
                var copy = new List<string>(this.stepsToReproduce);
                return copy;
            }
            private set
            {
                this.stepsToReproduce = value;
            }
        }

        public Priority Priority
        {
            get
            {
                return this.priority;
            }
            set
            {
                this.priority = value;
            }
        }

        public Severity Severity
        {
            get
            {
                return this.severity;
            }
            set
            {
                this.severity = value;
            }
        }

        public BugStatus Status
        {
            get
            {
                return this.status;
            }
            set
            {
                this.status = value;
            }
        }

        public IUser Assignee
        {
            get
            {
                return this.assignee;
            }
            set
            {
                this.assignee = value;
            }
        }
    }
}
