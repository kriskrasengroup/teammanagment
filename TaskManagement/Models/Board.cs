﻿using System.Collections.Generic;
using TaskManagement.Common;
using TaskManagement.Contracts;

namespace TaskManagement.Models
{
    public class Board : IBoard
    {
        private string name;
        private IList<IWorkItem> workItems = new List<IWorkItem>();

        public Board(string name)
        {
            this.Name = name;
        }

        public string Name
        {
            get => this.name;
            private set
            {
                Validator.ValidateArgumentIsNotNull(value, Constants.BOARD_NULL_ERR);
                Validator.ValidateIntRange(value.Length, Constants.BOARD_NAME_LEN_MIN, Constants.BOARD_NAME_LEN_MAX, Constants.BOARD_NAME_LEN_ERR);

                this.name = value;
            }
        }

        public IList<IWorkItem> WorkItems
        {
            get
            {
                var copy = new List<IWorkItem>(this.workItems);
                return copy;
            }
        }

        public void AddWorkItem(IWorkItem workItem)
        {
            this.workItems.Add(workItem);
        }
    }
}

