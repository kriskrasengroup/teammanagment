﻿namespace TaskManagement.Contracts
{
    public interface IActivityHistory
    {
        IUser User { get; }

        IBoard Board { get; }

        ITeam Team { get; }

        IWorkItem WorkItem { get; }
    }
}