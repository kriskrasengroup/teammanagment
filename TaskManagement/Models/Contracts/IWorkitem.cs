﻿using System.Collections.Generic;

namespace TaskManagement.Contracts
{
    public interface IWorkItem
    {
        public string Title { get; }

        public int Id { get; }

        public string Description { get; }

        IList<IComment> Comment { get; }

        public abstract void AddComment(string comment, string author);
    }
}



