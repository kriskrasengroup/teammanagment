﻿using TaskManagement.Common;
using TaskManagement.Contracts;
using TaskManagement.Enums;

namespace TaskManagement.Models
{
    public class Feedback : WorkItem, IFeedback
    {
        private int rating;
        private FeedbackStatus status;

        public Feedback(int id, string title, string description, int rating, FeedbackStatus status)
            : base(title, id, description)
        {
            Validator.ValidateIntRange(rating, Constants.RATING_MINIMUM_VALUE, Constants.RATING_MAXIMUM_VALUE, Constants.RATING_VALUE_ERR);

            this.Rating = rating;
            this.Status = status;
        }
        public int Rating
        {
            get
            {
                return this.rating;
            }
            set
            {
                this.rating = value;
            }
        }

        public FeedbackStatus Status
        {
            get
            {
                return this.status;
            }
            set
            {
                this.status = value;
            }
        }
    }
}
