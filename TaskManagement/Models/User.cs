﻿using System.Collections.Generic;
using TaskManagement.Common;
using TaskManagement.Contracts;

namespace TaskManagement.Models
{
    public class User : IUser
    {
        private string name;
        private IList<IWorkItem> workItems = new List<IWorkItem>();

        public User(string name)
        {
            this.Name = name;
        }

        public string Name
        {
            get => this.name;
            private set
            {
                Validator.ValidateArgumentIsNotNull(value, Constants.USER_NULL_ERR);
                Validator.ValidateIntRange(value.Length, Constants.USER_NAME_LEN_MIN, Constants.USER_NAME_LEN_MAX, Constants.USER_NAME_LEN_ERR);

                this.name = value;
            }
        }

        public IList<IWorkItem> WorkItems
        {
            get
            {
                var copy = new List<IWorkItem>(this.workItems);
                return copy;
            }
        }

        public void AddWorkItem(IWorkItem item)
        {
            this.workItems.Add(item);
        }

        public void RemoveWorkItem(IWorkItem item)
        {
            this.workItems.Remove(item);
        }
    }
}
