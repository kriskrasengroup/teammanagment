﻿using TaskManagement.Contracts;

namespace TaskManagement
{
    public class ActivityHistory : IActivityHistory
    {
        private IUser user;
        private IBoard board;
        private IWorkItem workItem;
        private ITeam team;

        public ActivityHistory(IUser user, ITeam team, IBoard board, IWorkItem workItem)
        {
            this.User = user;
            this.Team = team;
            this.Board = board;
            this.WorkItem = workItem;
        }

        public IUser User
        {
            get => this.user;
            private set
            {
                this.user = value;
            }
        }

        public ITeam Team
        {
            get => this.team;
            private set
            {
                this.team = value;
            }
        }

        public IBoard Board
        {
            get => this.board;
            private set
            {
                this.board = value;
            }
        }

        public IWorkItem WorkItem
        {
            get => this.workItem;
            private set
            {
                this.workItem = value;
            }
        }
    }
}
