﻿using System.Collections.Generic;
using TaskManagement.Common;
using TaskManagement.Contracts;

namespace TaskManagement.Models
{
    public class Team : ITeam
    {
        private string name;
        private IList<IUser> users = new List<IUser>();
        private IList<IBoard> boards = new List<IBoard>();

        public Team(string name)
        {
            this.Name = name;
        }

        public string Name
        {
            get => this.name;
            private set
            {
                Validator.ValidateArgumentIsNotNull(value, Constants.TEAM_NULL_ERR);
                Validator.ValidateIntRange(value.Length, Constants.TEAM_NAME_LEN_MIN, Constants.TEAM_NAME_LEN_MAX, Constants.TEAM_NAME_LEN_ERR);

                this.name = value;
            }
        }

        public IList<IUser> Users
        {
            get
            {
                var copy = new List<IUser>(this.users);
                return copy;
            }
        }

        public IList<IBoard> Boards
        {
            get
            {
                var copy = new List<IBoard>(this.boards);
                return copy;
            }
        }

        public void AddBoard(IBoard board)
        {
            this.boards.Add(board);
        }

        public void AddUser(IUser user)
        {
            this.users.Add(user);
        }
    }
}
