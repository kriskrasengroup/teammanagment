﻿using TaskManagement.Contracts;

namespace TaskManagement.Models
{
    public class Comment : IComment
    {
        private string comment;
        private string author;

        public Comment(string comment, string author)
        {
            this.Author = author;
            this.Content = comment;
        }

        public string Author
        {
            get
            {
                return this.author;
            }
            private set
            {
                this.author = value;
            }
        }
        public string Content
        {
            get
            {
                return this.comment;
            }
            private set
            {
                this.comment = value;
            }
        }
    }
}
