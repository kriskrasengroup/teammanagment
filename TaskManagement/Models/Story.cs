﻿using TaskManagement.Contracts;
using TaskManagement.Enums;

namespace TaskManagement.Models
{
    public class Story : WorkItem, IStory
    {
        private Size size;
        private StoryStatus status;
        private IUser assignee;
        private Priority priority;

        public Story(int id, string title, string description, Priority priority, Size size, StoryStatus status)
            : base(title, id, description)
        {
            this.Priority = priority;
            this.Size = size;
            this.Status = status;
        }

        public Priority Priority
        {
            get
            {
                return this.priority;
            }
            set
            {
                this.priority = value;
            }
        }
        public Size Size
        {
            get
            {
                return this.size;
            }
            set
            {
                this.size = value;
            }
        }

        public StoryStatus Status
        {
            get
            {
                return this.status;
            }
            set
            {
                this.status = value;
            }
        }

        public IUser Assignee
        {
            get
            {
                return this.assignee;
            }
            set
            {
                this.assignee = value;
            }
        }
    }
}
