﻿using System.Collections.Generic;
using TaskManagement.Common;
using TaskManagement.Contracts;

namespace TaskManagement.Models
{
    public abstract class WorkItem : IWorkItem
    {
        private string title;
        private string description;
        private int id;
        private IList<IComment> comment = new List<IComment>();

        public WorkItem(string title, int id, string description)
        {
            this.Title = title;
            this.Description = description;
            this.Id = id;
        }

        public string Title
        {
            get => this.title;
            private set
            {
                Validator.ValidateArgumentIsNotNull(value, Constants.TITLE_NULL_ERR);
                Validator.ValidateIntRange(value.Length, Constants.TITLE_NAME_LEN_MIN, Constants.TITLE_NAME_LEN_MAX, Constants.TITLE_NAME_LEN_ERR);

                this.title = value;
            }
        }

        public string Description
        {
            get => this.description;
            private set
            {
                Validator.ValidateArgumentIsNotNull(value, Constants.DESCRIPTION_NULL_ERR);
                Validator.ValidateIntRange(value.Length, Constants.DESCRIPTION_LEN_MIN, Constants.DESCRIPTION_LEN_MAX, Constants.DESCRIPTION_LEN_ERR);

                this.description = value;
            }
        }

        public int Id
        {
            get
            {
                return this.id;
            }
            private set
            {
                this.id = value;
            }
        }

        public IList<IComment> Comment
        {
            get
            {
                var copy = new List<IComment>(this.comment);
                return copy;
            }
        }

        public virtual void AddComment(string comment, string author)
        {
            var commentToAdd = new Comment(comment, author);
            this.comment.Add(commentToAdd);
        }
    }
}
